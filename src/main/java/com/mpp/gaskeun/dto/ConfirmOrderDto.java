package com.mpp.gaskeun.dto;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class ConfirmOrderDto {
    private String bookingMessage;
}
